import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RutasModule } from './app.rutas';
import { NoEncontradoComponent } from './no-encontrado/no-encontrado.component';

import { HomeComponent } from './home/home.component';
import { InicioFAQComponent } from './home/inicio-faq/inicio-faq.component';
import { InicioContactoComponent } from './home/inicio-contactos/inicio-contacto.component';

import { EquipoModule } from './equipo/equipo.module';
import { JugadorModule } from './jugador/jugador.module';


@NgModule({
  declarations: [
    AppComponent,
    NoEncontradoComponent,
    HomeComponent,
    InicioFAQComponent,
    InicioContactoComponent,
  ],


  imports: [
    BrowserModule,
    RutasModule,
    EquipoModule,
    JugadorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
