import { NgModule } from "@angular/core";
import { JugadorLista } from "../jugador/jugador-listar/jugador-listar.component";


@NgModule({
    imports:[],
    declarations:[JugadorLista],
    exports:[JugadorLista],
    providers:[]
})
export class ModuloComun{

}