import { NgModule } from "@angular/core";
import { RouterModule, Router, Route } from "@angular/router";
import { NoEncontradoComponent } from "./no-encontrado/no-encontrado.component";
import { HomeComponent } from "./home/home.component";
import { InicioFAQComponent } from './home/inicio-faq/inicio-faq.component';
import { AppComponent } from "./app.component";


const rutas: Route[] = [


    {
        path: 'equipo',
        loadChildren: 'src/app/equipo/equipo.module#EquipoModule'
    },
    
    {
        path: 'jugador',
        loadChildren: 'src/app/jugador/jugador.module#JugadorModule'
    },
    
    {
        path: 'home',
        component: HomeComponent,
        children: 
        [
            {
                path: 'FAQ',
                component: InicioFAQComponent
            }
        ]

    },


    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: '**',
        component: NoEncontradoComponent
    }

]

@NgModule({
    imports: [RouterModule.forRoot(rutas, {useHash: true})],
    exports: [RouterModule]
})
export class RutasModule { }