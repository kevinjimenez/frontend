import { NgModule } from "@angular/core";
import { Route, RouterModule } from "@angular/router";
import { EquipoCrearComponent } from "./equipo-crear/equipo-crear.component";
import { EquipoInfoComponent } from "./equipo-info/equipo-info.component";
import { EquipoListarComponent } from "./equipo-listar/equipo-listar.component";

const arregloDeRutasJugador:Route[] =
[
    {
        path: 'crear-equipo',
        component: EquipoCrearComponent,
        outlet: 'equipo'
    },
    {
        path: 'info-equipo',
        component: EquipoInfoComponent,
        outlet: 'equipo'
    },
    {
        path: 'listar-equipo',
        component: EquipoListarComponent,
        outlet: 'equipo'
    },
    {
        path: '',
        redirectTo: 'info-equipo',
        pathMatch: 'full'
    }
]

@NgModule({
    imports:[RouterModule.forChild(arregloDeRutasJugador)],
    exports:[RouterModule]
})

export class EquipoRutasModule{}