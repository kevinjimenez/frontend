import { NgModule } from "@angular/core";
import { EquipoCrearComponent } from "./equipo-crear/equipo-crear.component";
import { EquipoListarComponent } from "./equipo-listar/equipo-listar.component";
import { EquipoInfoComponent } from "./equipo-info/equipo-info.component";
import { EquipoRutasModule } from "./equipo.rutas";

@NgModule({
  
    declarations:
    [ 
        EquipoCrearComponent,
        EquipoListarComponent,
        EquipoInfoComponent,
    ],
    exports:[],
    imports:[EquipoRutasModule],
    providers:[]
})
export class EquipoModule{}