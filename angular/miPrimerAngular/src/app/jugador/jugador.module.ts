import { NgModule } from '@angular/core';
import { JugadorCrearComponent } from './jugador-crear/jugador-crear.component';
import { JugadorInfoComponent } from './jugador-info/jugador-info.component';

import { JugadorLista } from './jugador-listar/jugador-listar.component';
import { JugadorRutas } from './jugador.rutas';

@NgModule({
    declarations: [
        JugadorInfoComponent,
        JugadorLista,
        JugadorCrearComponent
        
    ],
    imports: [JugadorRutas],
    exports: [     
    ],
    providers: [],
})
export class JugadorModule {}