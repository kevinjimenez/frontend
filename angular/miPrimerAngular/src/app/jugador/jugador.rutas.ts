import { NgModule } from "@angular/core";
import { RouterModule, Router, Route } from "@angular/router";
import { JugadorCrearComponent } from "./jugador-crear/jugador-crear.component";
import { JugadorLista } from "./jugador-listar/jugador-listar.component";
import { JugadorInfoComponent } from "./jugador-info/jugador-info.component";


const arregloDeRutasHijos: Route[] = 

[
    {
        path: 'crear-jugador',
        component: JugadorCrearComponent,
        outlet: 'jugador'
    },
    {
        path: 'listar-jugadores',
        component: JugadorLista,
        outlet: 'jugador'
    },
    {
        path: 'info-jugador',
        component: JugadorInfoComponent,
        outlet: 'jugador'
    },
    {
        path: '',
        redirectTo: 'crear-jugador',
        pathMatch: 'full'
    },


]

@NgModule({

    imports: [RouterModule.forChild(arregloDeRutasHijos)],
    exports: [RouterModule]
})

export class JugadorRutas {}

